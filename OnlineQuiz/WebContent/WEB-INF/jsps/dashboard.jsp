<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<SCRIPT language="javascript">
function add(content) {
 
    //Create an input type dynamically.
    var element = document.createElement("li");
    element.setAttribute("class", "large-3 columns");
    var textelement=document.createElement("textarea");
    var buttonelement=document.createElement("button");
    var buttonelement1=document.createElement("button");
    var t = document.createTextNode("Delete");
    var t1 = document.createTextNode("Save");
    buttonelement.appendChild(t);
    buttonelement.setAttribute("class", "delete_button");
    buttonelement1.appendChild(t1);
    buttonelement.setAttribute("onClick", "delete_li(this)");
    textelement.setAttribute("class", "textnote");
    var text_box = document.createTextNode(content);
    textelement.appendChild(text_box);
    //Assign different attributes to the element.
  //  element.setAttribute("type", type);
   //element.setAttribute("value", textelement);
   // element.setAttribute("name", type);
   element.appendChild(buttonelement);
   element.appendChild(textelement);
 //  element.appendChild(buttonelement1);
   
  
    element.setAttribute("class", "ui-state-default");
 //	element.setAttribute("ui-state-default", class);
 
    var foo = document.getElementById("sortable");
 
    //Append the element in page (in span).
    foo.appendChild(element);
 
}
</SCRIPT>


<link rel="stylesheet" href="${pageContext.request.contextPath}/css/foundation.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css" />
    <script src="${pageContext.request.contextPath}/js/modernizr.js"></script>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 
  <style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 1050px; }
  #sortable li { margin: 10px 10px 10px 0; padding: 1px; float: left; min-width: 300px; min-height: 230px; font-size: 4em; text-align: center; }
  </style>
  <script>
  $(function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  });
  </script>
  <script>
  $(function() {
    $( "#draggable" ).draggable({ cursor: "move", cursorAt: { top: 56, left: 56 } });
    $( "#draggable2" ).draggable({ cursor: "crosshair", cursorAt: { top: -5, left: -5 } });
    $( "#draggable3" ).draggable({ cursorAt: { bottom: 0 } });
  });
  </script>

<script>
function combine() {
  
    var slides = document.getElementsByClassName('textnote');
    var answer="",i;
    for(i = 0; i < slides.length; i++)
    {
       answer=answer+"//"+slides[i].value;
    }
    var foo1 = document.getElementById("textarea1");
    var t1 = document.createTextNode(answer);
    foo1.appendChild(t1);
}
</script>


<script>
function refresh() {
  	combine();
  
    $('.ui-state-default').remove();
    
    var str = "//How//are//you//doing//today?";
    var res = str.split("//");
    for(var i = 1; i < res.length; i++)
    {
       add(res[i]);
    }
}
</script>

<script>
function initialLoad(title) {
  	 
    var str = '${datatext}';
    var res = str.split("//");
    for(var i = 1; i < res.length; i++)
    {
       add(res[i]);
    }
   
    
}
</script>

<script>
function delete_li(e)
{
    e.parentNode.parentNode.removeChild(e.parentNode);
}
</script>



</head>
<body onload="initialLoad()">
<div id='cssmenu'>
<ul>
   <li class=''><a href='${pageContext.request.contextPath}'><span>Home</span></a></li>
    <li class=''><a href='${pageContext.request.contextPath}/logout'><span>Logout</span></a></li>
</ul>
</div>

<div class="row" id="main">
<div class="large-12 columns" id="header">
<div class="large-9 columns"><h1 class="gray_title" style="font-size:48px">Study Genie Guide</h1></div>
<div class="large-3 columns" style="text-align:right">


</div>
</div>

<div class="row" id="content">
<div class="large-3 columns" id="left">
<a class="round button" href='${pageContext.request.contextPath}/progress_bar' style="background-color:green;width:48%">My Progress</a>
<a class="round button" href='${pageContext.request.contextPath}/comparative_progress' style="background-color:green;width:48%">Comparative Progress</a>
<a class="round button" href='${pageContext.request.contextPath}/dashboard_chart' style="background-color:green;width:100%">Analysis</a>
<form name="title-select" action="retrieve" method="post">

<a class="round button" href="retrieve?test=object_and_classes">object_and_classes</a>
<a class="round button" href="retrieve?test=Basic_DataTypes">Basic DataTypes</a>
<a class="round button" href="retrieve?test=variable">Variable</a>
<a class="round button" href="retrieve?test=Loops_and_Decision_making">Loops and Decision making</a>
<a class="round button" href="retrieve?test=String">String</a>
<a class="round button" href="retrieve?test=Arrays">Arrays</a>
<a class="round button" href="retrieve?test=Regular_Expression">Regular Expression</a>
<a class="round button" href="retrieve?test=Methods">Methods</a>
<a class="round button" href="retrieve?test=Exceptions">Exceptions</a>
<a class="round button" href="retrieve?test=Inherence">Inheritance</a>
<a class="round button" href="retrieve?test=Overloading">Overloading</a>
<a class="round button" href="retrieve?test=Interface">Interface</a>
<a class="round button" href="retrieve?test=Packages">Packages</a>
<a class="round button" href="retrieve?test=Generics">Generics</a>
</form>


</div>
<div class="large-9 columns" id="right">
<div class="row" id="right_top">



 
<button class="round buton " onclick="add('')">Add Note</button>





</div>

<form name="notes-save" action="save" method="post">
<input type="hidden" name="title_label" value="${datatitle}"/>
<h3>${datatitle}</h3>
<div class="row" id="right_middle">


<ul id="sortable" style="margin-left:50px">
  
</ul>



</div>
<div class="row" id="right_bottom">

<input type="submit" name="submit" onclick="combine()" value="Save" class="button" />

<h3>Recommendations</h3>
<ul>
<li><a target="_blank" href="${r_one}">${r_one}</a></li>
<li><a target="_blank" href="${r_second}">${r_second}</a></li>
<li><a target="_blank" href="${r_third}">${r_third}</a></li>

</ul>
  <TEXTAREA style="visibility:hidden" name="textarea1" id="textarea1" ROWS="5"></TEXTAREA>
    <TEXTAREA style="visibility:hidden" name="textarea3" id="textarea3" ROWS="5"></TEXTAREA>
</div>

</form>

</div>
</div>

</div>
<script>
function set_title(e)
{
	var a = document.getElementById("post");
    a.value = e;
}
</script>
</body>
</html>