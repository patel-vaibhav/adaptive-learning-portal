
package utility;

import java.io.IOException;

import dao.DatabaseConnectors;
import pojo.UserNotes;
import pojo.UserInfo;

public class Score {
	
	DatabaseConnectors dbcon = new DatabaseConnectors();
	
	public float calculateScore(UserNotes uNotes)
	{
		UserInfo uInfo = dbcon.getUserInfoByUsername(uNotes.getUserId());
		
		float p1 = uInfo.getQuizScore();
		
		String[] notes = uNotes.getNotes().split("//");
		
		int note_c = notes.length;
		int word_count = 0;
		
		for(int i=0; i<notes.length; i++){
			word_count += countWords(notes[i]);
		}
		
		float p2 = (25 * (note_c * word_count) + 5)/( (note_c * word_count) + 5);
		
		float p3 = 0;
		
		try {
			p3 = Simi.calculateSimilarity(uNotes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			p3 = 0;
		}
		
		System.out.println("p1" + p1);
		System.out.println("p2" + p2);
		System.out.println("p3" + p3);
		
		float score = p1 + p2 + ((p3 * 100)/2);
		
		System.out.println("score" + score);
		
		return score;
	}
	

	// Code Courtesy: http://stackoverflow.com/questions/5864159/count-words-in-a-string-method
	public static int countWords(String s){
	
	    int wordCount = 0;
	
	    boolean word = false;
	    int endOfLine = s.length() - 1;
	
	    for (int i = 0; i < s.length(); i++) {
	        // if the char is a letter, word = true.
	        if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
	            word = true;
	            // if char isn't a letter and there have been letters before,
	            // counter goes up.
	        } else if (!Character.isLetter(s.charAt(i)) && word) {
	            wordCount++;
	            word = false;
	            // last word of String; if it doesn't end with a non letter, it
	            // wouldn't count without this.
	        } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
	            wordCount++;
	        }
	    }
	    return wordCount;
	}


	public void SaveTopicScore(UserInfo uInfo, String topicID, float score){
		
		int topicId = Simi.getTopicIndex(topicID);
		
		if(topicId == 1){
			uInfo.setTs1(score);
		} else if(topicId == 2){
			uInfo.setTs2(score);
		} else if(topicId == 3){
			uInfo.setTs3(score);
		} else if(topicId == 4){
			uInfo.setTs4(score);
		} else if(topicId == 5){
			uInfo.setTs5(score);
		} else if(topicId == 6){
			uInfo.setTs6(score);
		} else if(topicId == 7){
			uInfo.setTs7(score);
		} else if(topicId == 8){
			uInfo.setTs8(score);
		} else if(topicId == 9){
			uInfo.setTs9(score);
		} else if(topicId == 10){
			uInfo.setTs10(score);
		} else if(topicId == 11){
			uInfo.setTs11(score);
		} else if(topicId == 12){
			uInfo.setTs12(score);
		} else if(topicId == 13){
			uInfo.setTs13(score);
		} else if(topicId == 14){
			uInfo.setTs14(score);
		}
		
	}
	
}
