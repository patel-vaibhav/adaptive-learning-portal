
package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import pojo.UserNotes;
import utility.keywordFinder.Keyword;

public class Simi {

	static public String[] javatokens = {"Toolkit","abstract","abstract class","abstract method","API","appliances","applet","argument","array","ASCII","atomic","Bean","binary","bit","bitwise","block","boolean","bounding","break","byte","bytecode","case","casting","catch","char","class","class method","classpath","client","codebase","comment","compilation","compiler","compositing","constructor","const","continue","core","Packages","critical","declaration","default","definition","deprecation","derived","distributed","do","double","precision","else","EmbeddedJava","encapsulation","exception","exception","handler","executable","extends","FCS","field","final","finally","float","for","FTP","formal","garbage","goto","GUI","hexadecimal","hierarchy","HotJava","HTML","HTTP","IDL","identifier","if","implements","import","inheritance","instance","instanceof","int","interface","Internet","IP","interpreter","JAE","JAR","Environment","JAE","JavaBeans","JavaBlend","Card","JavaCheck","JavaChip","java","Compatibility","JCK","kit","JDBC","JDK","JavaOS","JavaPlan","Platform","RMI","JavaSafe","JavaScript","Studio","Server","wallet","JavaSpaces","JavaSoft","Jini","JMAPI","JNDI","JPEG","JRE","JIT","keyword","lexical","linker","literal","local","long","member","method","Mosaic","multithreaded","native","NCSA","new","null","object","object-oriented design","octal","Optional Packages","overloading","overriding","package","peer","pixel","POSIX","private","process","property","Profiles","protected","public","raster","reference","return","RFE","root","RPC","runtime","Sandbox","scope","SSL","servlet","short","SGML","static","subarray","subclass","subtype","superclass","super","supertype","switch","Swing","synchronized","TCP/IP","Thin","this","thread","throw","throws","transient","try","type","Unicode","URL","variable","virtual machine","void","volatile","while","world readable files","wrapper","WWW"};
	static HashMap<Integer, Integer[]> topicKeywordScore = new HashMap<Integer, Integer[]>();
	static String[][] topic_title_and_frequency = {{"object_and_classes","Object Lifecycle/Classes, Objects and Types"}, {"Basic_DataTypes","Primitive Types"},
						{"variable","Variables"}, {"Loops_and_Decision_making","Loop blocks"},
						{"String", "String"}, {"Arrays","Arrays"},
						{"Regular_Expression","Regular Expressions"}, {"Methods","Methods"},
						{"Exceptions","Unchecked Exceptions/Checked Exceptions/Throwing and Catching Exceptions"}, {"Inherence","Inheritance"},
						{"Overloading",""}, {"Interface", ""},
						{"Packages","Overloading and Overriding"}, {"Generics","Generics"}};
	
	static public String[][] topic_r = { { "http://www.tutorialspoint.com/java/java_object_classes.htm", "http://docs.oracle.com/javase/tutorial/java/javaOO/index.html", "https://en.wikibooks.org/wiki/Java_Programming/Defining_Classes"},
			
								   { "http://www.tutorialspoint.com/java/java_basic_datatypes.htm", "http://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html", "https://en.wikibooks.org/wiki/Java_Programming/Primitive_Types"},
	
								   { "http://www.tutorialspoint.com/java/java_variable_types.htm", "http://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html", "https://en.wikibooks.org/wiki/Java_Programming/Variables"},
								   
								   { "http://www.tutorialspoint.com/java/java_loop_control.htm", "http://docs.oracle.com/javase/tutorial/java/nutsandbolts/flow.html", "https://en.wikibooks.org/wiki/Java_Programming/Loop_blocks"},
								   
								   { "http://www.tutorialspoint.com/java/java_strings.htm", "http://docs.oracle.com/javase/tutorial/java/data/strings.html", "https://en.wikibooks.org/wiki/Java_Programming/API/java.lang.String"},
								   
								   { "http://www.tutorialspoint.com/java/java_arrays.htm", "http://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html", "https://en.wikibooks.org/wiki/Java_Programming/Arrays"},
								   
								   { "http://www.tutorialspoint.com/java/java_regular_expressions.htm", "https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html", "https://en.wikibooks.org/wiki/Java_Programming/Regular_Expressions"},
								   
								   { "http://www.tutorialspoint.com/java/java_methods.htm", "http://docs.oracle.com/javase/tutorial/java/javaOO/methods.html", "https://en.wikibooks.org/wiki/Java_Programming/Methods"},
								   
								   { "http://www.tutorialspoint.com/java/java_exceptions.htm", "https://docs.oracle.com/javase/tutorial/essential/exceptions/", "https://en.wikibooks.org/wiki/Java_Programming/Throwing_and_Catching_Exceptions"},
								   
								   { "http://www.tutorialspoint.com/java/java_inheritance.htm", "http://docs.oracle.com/javase/tutorial/java/concepts/inheritance.html", "https://en.wikibooks.org/wiki/Java_Programming/Inheritance"},
								   
								   { "http://www.tutorialspoint.com/java/java_overriding.htm", "https://docs.oracle.com/javase/tutorial/java/IandI/override.html", "http://beginnersbook.com/2014/01/method-overriding-in-java-with-example/"},
								   
								   { "http://www.tutorialspoint.com/java/java_interfaces.htm", "http://docs.oracle.com/javase/tutorial/java/concepts/interface.html", "https://en.wikibooks.org/wiki/Java_Programming/Interfaces"},
								   
								   { "http://www.tutorialspoint.com/java/java_packages.htm",  "http://docs.oracle.com/javase/tutorial/java/concepts/package.html", "http://www.studytonight.com/java/package-in-java.php"},
								   
								   { "http://www.tutorialspoint.com/java/java_generics.htm", "http://docs.oracle.com/javase/tutorial/java/generics/index.html", "https://en.wikibooks.org/wiki/Java_Programming/Generics"}
								 };		
	
	static float calculateSimilarity(UserNotes uNotes) throws IOException{
		
		int[] noteVector = new int[javatokens.length];
		Integer[] similarVector = new Integer[javatokens.length];
		
		for(int i=0; i<javatokens.length; i++){
			similarVector[i] = 0;
		}
		
		String[] notes = uNotes.getNotes().split("//");
		
		StringBuffer d = new StringBuffer();
		
		for(int i=0; i<notes.length; i++){
			d.append(notes[i]);
			d.append(".");
		}
		
		List<Keyword> keywords = null;
		
		try {
			keywords = keywordFinder.findKeyword(d.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("From calculateSimilarity 1\n\n");
			e.printStackTrace();
			return 0;
		}
		
		for(Keyword k : keywords){
			for(String t : k.getTerms()){
				for(int i=0; i<javatokens.length; i++){
					if(t.contains(javatokens[i]) || javatokens[i].contains(t)){
						noteVector[i]++;
					}
				}
			}
		}
		
		int topicID = getTopicIndex(uNotes.getTopicID());
		
		StringBuffer stringBuffer = new StringBuffer();
		
		if(topicKeywordScore.containsKey(topicID) == false){
			
			String[] file_names = topic_title_and_frequency[topicID-1][1].split("/");
			
			String line_1 = null;
			
			for(int i=0; i<file_names.length; i++){
					
				File f = new File(System.getProperty("user.dir") + "/crawlfiles/" + file_names[i] + ".txt");
				
				FileInputStream is = new FileInputStream(f);
		        BufferedReader r = new BufferedReader(new InputStreamReader(is));
		        
		        while((line_1 = r.readLine())!=null){
		          stringBuffer.append(line_1);
		        }
			}
		
			try {
				keywords = keywordFinder.findKeyword(stringBuffer.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("From calculateSimilarity 1\n\n");
				e.printStackTrace();
				return 0;
			}
		
			for(Keyword k : keywords){
				for(String t : k.getTerms()){
					for(int i=0; i<javatokens.length; i++){
						if((t != null) && (t.contains(javatokens[i]) || javatokens[i].contains(t))){
							similarVector[i]++;
						}
					}
				}
			}
			
			topicKeywordScore.put(topicID, similarVector);
		
		}else{
			
			similarVector = topicKeywordScore.get(topicID);
		}
		
		
		// Cosine similarity calculation
		
		float x_y = 0;
		
		for(int i=0; i<similarVector.length; i++){
			x_y += (noteVector[i] * similarVector[i]);
		}
		
		float mod_x = 0;
		
		for(int i=0; i<noteVector.length; i++){
			mod_x += (noteVector[i] * noteVector[i]);
		}
		
		mod_x = (float) Math.sqrt(mod_x);
		
		float mod_y = 0;
		
		for(int i=0; i<similarVector.length; i++){
			mod_y += (similarVector[i] * similarVector[i]);
		}
		
		mod_y = (float) Math.sqrt(mod_y);
		
		if(mod_x == 0 || mod_y==0){
			return 0;
		}
		
		float cosine = x_y / (mod_x * mod_y);
		
		return cosine;
	}
	
	public static int getTopicIndex(String topic){
		
		for(int i=0; i<topic_title_and_frequency.length; i++){
			if(topic.equals(topic_title_and_frequency[i][0])){
				return i+1;
			}
		}
		
		return 0;
		
	}
}
