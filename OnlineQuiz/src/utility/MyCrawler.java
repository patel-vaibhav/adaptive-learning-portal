package utility;

import java.util.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MyCrawler {

	protected Queue<String> Q;
	protected String rootstr;
	
	/* Crawler class based on https://en.wikibooks.org/wiki/Java_Programming */
	
	public MyCrawler(String s){
		rootstr = new String(s);
		
		Q = new LinkedList<String>();
		Q.add(s);
	}
	
	public void doCrawling() throws Exception
	{
		parseAllhref();
		crawlAllURL();
	}
	
	protected void parseAllhref() throws Exception
	{
		int i=0;
		
		if(Q==null){
			throw new NullPointerException("Queue object is not initilized");
		}
		
		if(Q.size() == 0){
			throw new Exception("Queue is empty");
		}
		
		Document doc = Jsoup.connect(Q.remove()).get();
		Elements heading = doc.select("#firstHeading");
		
		//File file = new File("C:\\Directory1");
		File dir = new File(System.getProperty("user.dir") + "/crawlfiles");
		if (!dir.exists()) {
			if (dir.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
		
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir") + "/crawlfiles/" + heading.text()+".txt"), "utf-8"));
		
		Elements content = doc.select("#mw-content-text p");
		
		writer.write(content.text());
		writer.close();
		
		Elements href = doc.select("#content ul a[href]");
		
		//href.attr("href");
		
		if(href.size() > 6){
			for(i=0; i<6; i++){
				href.remove(0);
			}
		}
		
		for(i=0; i<href.size(); i++){
			if(href.get(i).text().isEmpty()){
				href.remove(i);
				i--;
			}
		}
		
		for(i=0; i<16; i++){
			href.remove( href.size() - 1);
		}
		
		for(i=0; i<href.size(); i++){
			//System.out.println(href.get(i).text() + ":" + href.get(i).attr("href"));
			//System.out.println();
			Q.add(href.get(i).attr("href"));
			//break;
		}
	}
	
	protected void crawlAllURL() throws Exception
	{	
		System.out.println("crawlAllURL inside");
		int i=0, size = 0;
		
		if(Q==null){
			throw new NullPointerException("Queue object is not initilized");
		}
		
		if(Q.size() == 0){
			throw new Exception("Queue is empty");
		}
		
		size = Q.size();
		
		System.out.println("Size of q is:"+Q.size());
		
		for(i=0; i<size; i++){
			String url = "https://en.wikibooks.org" + Q.remove();
			
			Document doc = Jsoup.connect(url).get();
			
			Elements heading = doc.select(".firstHeading");
			
			Element etemp = null;
			
			for(Element e1: heading){
				System.out.println("url is:  " + url + "\n" + e1.text());
				etemp = e1;
				break;
			}
			
			String filename = etemp.text().replace("/", "_");
			
			//System.out.println("url is:  " + url + "\n" + heading.text());
			
			Writer writer_html = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir") + "/crawlfiles/" + filename + ".html"), "utf-8"));
			
			writer_html.write(url);
			writer_html.close();
			
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir") + "/crawlfiles/" + filename + ".txt"), "utf-8"));
			//Writer writer2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir") + "/crawlfiles/" + filename + " html" + ".txt"), "utf-8"));
			
			//Elements tags = doc.select("#mw-content-text");
			//bodyContent
			
			Elements temp = doc.body().getAllElements();
			
			
			System.out.println("crawlAllURL out");
			
			
			for(Element el : temp)
			//for(int j=0; j<size; j++)
			{
				//Element el = tags.get(j);
				
				if(el.tagName().equals("p")){
					writer.write(el.text() + "\n");
					writer.write("\n");
				}
				
				
				if(el.tagName().equals("table") && !el.className().equals("wikitable") && !el.className().equals("noprint")){
					writer.write(el.text() + "\n");
					writer.write("\n");
				}
				
				
				if(el.tagName().equals("ul")){
					continue;
				}
				
				if(el.tagName().equals("li")){
					writer.write("- " + el.text());
				}
				
				if(el.tagName().equals("h2")){
					writer.write(el.text()+"\n");
					for(int k=0; k<el.text().length(); k++){
						writer.write("-");
					}
				}
				
				if(el.tagName().equals("h3")){
					writer.write(el.text()+"\n");
				}
			}
			writer.close();
			
		}
		
	}
	
}
