package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;
import utility.keywordFinder.Keyword;


public class getHighPage {

	public static String[] getHigeIndexPage(String[] notes){
		
		bunch b = null;
		
		String[] rst = new String[2];
		rst[0] = null;
		rst[1] = null;
		
		try {
			b = indexer.doIndexing();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String Note = null;
		
		StringBuffer sb = new StringBuffer();
	
		if(notes.length == 0 || notes[0].length() < 2){
			return null;
		}
		
		for(int i=0; i<notes.length; i++){
			sb.append(notes[i]);
			sb.append(".");
		}
		
		Note = sb.toString();
		
		int i=0, index = -1;
		
        try {
        	
        	Map<String,Integer> map = new TreeMap<String,Integer>();
			SortedMap<String, Integer> smap = new TreeMap<String, Integer>();
        	
			for(i=0; i<Simi.javatokens.length; i++){
				map.put(Simi.javatokens[i].toLowerCase(), 0);
				smap.put(Simi.javatokens[i].toLowerCase(), 0);
			}
        	
        	List<Keyword> keywords = keywordFinder.findKeyword(Note);
        	
        	for(Keyword k : keywords){
        		String ss = k.getStem();
        		
        		if(ss.length() < 3){
        			continue;
        		}
        		
        		SortedMap<String, Integer> tailMap = smap.tailMap(ss);
        		
        		if(!tailMap.isEmpty() && tailMap.firstKey().toLowerCase().startsWith(ss.toLowerCase())){
        			map.put(tailMap.firstKey().toLowerCase(), map.get(tailMap.firstKey().toLowerCase()) + k.getFrequency());
        		}
        	}
        	
        	/*for(Keyword k : keywords){
        		String ss = k.getStem();
        		
        		if(map.containsKey(ss)){
        			map.put(ss, map.get(ss) + k.getFrequency());
        		}
        		
        		for(String myterm : k.getTerms()){
        			if(map.containsKey(myterm)){
        				map.put(myterm, map.get(myterm) + 1);
        				continue;
        			}
        		}
        	}*/
        	
        	map = indexer.sortByValue(map);
        	
        	Map<String, Integer> keymap = new TreeMap<String, Integer>();
        	
        	for(String key : map.keySet()){
        		if(map.get(key) > 0){
        			System.out.println(key + " " + map.get(key));
        			keymap.put(key.toLowerCase(), map.get(key));
        		}
        	}
        	
        	int tempsize = keymap.size();
        	int h = 0;
        	String qstr = "";
        	
        	for(String key : map.keySet()){
        		if(map.get(key) > 0){
        			if(h >= (tempsize-2)){
        				qstr += key + " OR ";
        			}else{
        				h++;
        			}
        		}
        	}
        	
        	if(qstr.isEmpty()){
        		
        		for(String stem : keywords.get(0).getTerms()){
        			qstr += stem + " OR ";
        		}
        		
        	}
        	qstr = qstr.substring(0, qstr.length() - 4);
        	
        	System.out.println(qstr);
        	
        	//String stem = keywords.get(0).getStem();
        	String querystr = "contents:" + qstr;
        	
        	System.out.println(querystr);
        	System.out.println(keywords.get(0).getTerms());
    
    		Query q = new QueryParser(Version.LUCENE_48, "contents", b.analyzer).parse(querystr);
    		int hitsPerPage = 10;
    		IndexReader reader = null;
    		 
    		 
    		TopScoreDocCollector collector = null;
    		IndexSearcher searcher = null;
    		reader = DirectoryReader.open(b.indexDir);
    		searcher = new IndexSearcher(reader);
    		collector = TopScoreDocCollector.create(hitsPerPage, true);
    		searcher.search(q, collector);

    		 
    		ScoreDoc[] hits = collector.topDocs().scoreDocs;
    		System.out.println("Found " + hits.length + " hits.");
    		System.out.println();
    		 
    		/*for (i = 0; i < hits.length; ++i) {
    			int docId = hits[i].doc;
    			Document d;
    			d = searcher.doc(docId);
    			
    			System.out.println((i + 1) + ". " + d.get("filename"));
    		}*/
    		
    		String line="";
    		String line_1="";
    		
    		int hin = 0;
    		
    		for (i = 0; i < hits.length; ++i) {
    			
    			if(hin > 1){
    				break;
    			}
    			
    			line="";
        		line_1="";
    			
    			int docId = hits[i].doc;
    			Document dd;
    			dd = searcher.doc(docId);
    			
    			File f = new File(System.getProperty("user.dir") + "/crawlfiles/" + dd.get("filename") + ".html");
    			
    			FileInputStream is = new FileInputStream(f);
		        BufferedReader r = new BufferedReader(new InputStreamReader(is));
		        StringBuffer stringBuffer = new StringBuffer();
		        
		        while((line_1 = r.readLine())!=null){
		          stringBuffer.append(line_1).append("\n");
		        }
		        
		        line = stringBuffer.toString();
		        
		        rst[hin] = line;
		        
		        System.out.println("\n\n" + line);
    			//System.out.println(line);
                
    			is.close();
    			r.close();
    		}
    		
    		reader.close();            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return rst;
		
	}
	
}
