package utility;

import java.util.ArrayList;
import java.util.List;

import dao.DatabaseConnectors;
import pojo.UserInfo;

public class scoreboard {

	static DatabaseConnectors dbcon = new DatabaseConnectors();
	
	
	public static String getUserScoreArray(String username){
		
		UserInfo ui = dbcon.getUserInfoByUsername(username);
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("[");
		
		for(int i=0; i<14; i++){
			sb.append(String.valueOf(getScore(ui, i+1)));
			if(i!=13) {sb.append(",");}
		}
		sb.append("]");
		
		return sb.toString();
	}
	
	public static String topicMean(){
		
		List<UserInfo> uInfoList = dbcon.getAllUserInfo();
		
		float total = 0;
		
		StringBuffer sb = new StringBuffer();
		
		sb.append("[");
		
		for(int j=0; j<14; j++){
		
			int i=0;
			
			for(i=0; i<uInfoList.size(); i++){
				total += getScore(uInfoList.get(i), j+1);
			}
			
			sb.append(String.valueOf(total/uInfoList.size()));
			
			if(j!=13) {sb.append(",");}
		}
		
		sb.append("]");
		
		return sb.toString();
	}
	
	public static List<String> lowMediumHigh()
	{
		List<UserInfo> uInfoList = dbcon.getAllUserInfo();
		
		int low = 0, medium = 0, high = 0;
		
		StringBuffer sbl = new StringBuffer();
		StringBuffer sbm = new StringBuffer();
		StringBuffer sbh = new StringBuffer();
		
		sbl.append("[");
		sbm.append("[");
		sbh.append("[");
		
		for(int j=0; j<14; j++){
			
			int i=0;
			low = 0;
			medium = 0;
			high = 0;
			
			for(i=0; i<uInfoList.size(); i++){
				float s = getScore(uInfoList.get(i), j+1);
				
				if(s > 0 && s <= 20){
					low++;
				}else if(s > 20 && s <= 40){
					medium++;
				}else if(s>40){
					high++;
				}				
			}
			
			sbl.append(String.valueOf(low));
			sbm.append(String.valueOf(medium));
			sbh.append(String.valueOf(high));
			
			if(j!=13){
				sbl.append(",");
				sbm.append(",");
				sbh.append(",");
			}
			
		}
		
		sbl.append("]");
		sbm.append("]");
		sbh.append("]");
		
		List<String> rl = new ArrayList<String>();
		
		rl.add(sbl.toString());
		rl.add(sbm.toString());
		rl.add(sbh.toString());
		
		return rl;
	}
	
	static float getScore(UserInfo uInfo, int topicId){
			
		if(topicId == 1){
			return uInfo.getTs1();
		} else if(topicId == 2){
			return uInfo.getTs2();
		} else if(topicId == 3){
			return uInfo.getTs3();
		} else if(topicId == 4){
			return uInfo.getTs4();
		} else if(topicId == 5){
			return uInfo.getTs5();
		} else if(topicId == 6){
			return uInfo.getTs6();
		} else if(topicId == 7){
			return uInfo.getTs7();
		} else if(topicId == 8){
			return uInfo.getTs8();
		} else if(topicId == 9){
			return uInfo.getTs9();
		} else if(topicId == 10){
			return uInfo.getTs10();
		} else if(topicId == 11){
			return uInfo.getTs11();
		} else if(topicId == 12){
			return uInfo.getTs12();
		} else if(topicId == 13){
			return uInfo.getTs13();
		} else if(topicId == 14){
			return uInfo.getTs14();
		}
		
		return 0;
			
	}
	
}
