
package quiz.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DatabaseConnectors;
import pojo.Login;
import pojo.UserInfo;

/**
 * Servlet implementation class RegistrationController
 */
@WebServlet("/checkRegister")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	
	DatabaseConnectors dbcon = new DatabaseConnectors();
	
    public RegistrationController() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Login login = null;
		
		String username = request.getParameter("username");
		
		login = dbcon.getLoginByUsername(username);
		
		if(login != null){
			System.out.println("\n\n Coming here" + login.getUserId());
			
			request.setAttribute("errorMessage","Username already present");
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsps/register.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		String pass1 = request.getParameter("password");
		String pass2 = request.getParameter("reenter_password");
		
		if(pass1.isEmpty() || pass1.equals(null) || pass2.isEmpty() || pass2.equals(null) || pass1.equals(pass2) == false){
			request.setAttribute("errorMessage","Pasword mismatch");
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsps/register.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		login = new Login();
		
		login.setUserId( username);
		login.setEmail(request.getParameter("email"));
		login.setPasswd(pass1);
		
		UserInfo ui = new UserInfo();
		ui.setUserId(request.getParameter("username"));
		
		dbcon.saveLogin(login);
		dbcon.saveUserInfo(ui);
		
        request.setAttribute("newUser",login.getUserId());
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsps/regSuccess.jsp");
		dispatcher.forward(request, response);
	}

}
