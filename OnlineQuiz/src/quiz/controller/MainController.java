
package quiz.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DatabaseConnectors;
import pojo.UserInfo;
import pojo.UserNotes;
import utility.*;

@WebServlet(urlPatterns = { "/login", "/register", "/takeExam", "/logout", "/dashboard", "/save", "/retrieve", "/dashboard_chart", "/progress_bar", "/comparative_progress" })
public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	DatabaseConnectors dbcon = new DatabaseConnectors();
	
	static int is_crawled = 0;
	
	public MainController() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		
		if(is_crawled==0){
			MyCrawler mc = new MyCrawler("https://en.wikibooks.org/wiki/Java_Programming");
			try {
				mc.doCrawling();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			is_crawled = 1;
		}
	}
	
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
		
	}
	
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String applicationContextPath = request.getContextPath();

		if (request.getRequestURI().equals(applicationContextPath + "/")) {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/home.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/dashboard_chart")) {
			
			List<String> rl = scoreboard.lowMediumHigh();
			
			System.out.println("\n\rl l" + rl.get(0) + "\n");
			System.out.println("\n\rl 2" + rl.get(1) + "\n");
			System.out.println("\n\rl 3" + rl.get(2) + "\n");
			
			request.setAttribute("low", rl.get(0));
			request.setAttribute("medium", rl.get(1));
			request.setAttribute("high", rl.get(2));
			
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/dashboard_chart.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/progress_bar")) {
			
			String sArray = scoreboard.getUserScoreArray((String)request.getSession().getAttribute("user"));
			
			System.out.println("\n\nsArray" + sArray + "\n");
			
			request.setAttribute("user_pro", sArray);
			
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/myprogressBar.jsp");
			dispatcher.forward(request, response);
			
		}
		else if (request.getRequestURI().equals(
				applicationContextPath + "/comparative_progress")) {
			
			String sArray = scoreboard.getUserScoreArray((String)request.getSession().getAttribute("user"));
			
			System.out.println("\n\nsArray" + sArray + "\n");
			
			
			
			String mArray = scoreboard.topicMean();
			
			System.out.println("\n\nmArray" + sArray + "\n");
			
			request.setAttribute("user_pro", sArray);
			request.setAttribute("user_mean", mArray);
			
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/horizontal_bar_chart.jsp");
			dispatcher.forward(request, response);
		}
		else if (request.getRequestURI().equals(
				applicationContextPath + "/login")) {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/login.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/register")) {
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/register.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/dashboard")) {
			
			System.out.println(request.getSession().getAttribute("user"));
			if (request.getSession().getAttribute("user") == null) {
				request.getRequestDispatcher("/login").forward(request,
						response);
				
			}else{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/WEB-INF/jsps/dashboard.jsp");
				dispatcher.forward(request, response);
			}
			
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/takeExam")) {
			request.getSession().setAttribute("currentExam", null);

			String exam = request.getParameter("test");
			
			request.getSession().setAttribute("exam", exam);

			System.out.println(request.getSession().getAttribute("user"));
			if (request.getSession().getAttribute("user") == null) {
				request.getRequestDispatcher("/login").forward(request,
						response);
				
			} else 
			
			{
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/WEB-INF/jsps/quizDetails.jsp");
				dispatcher.forward(request, response);
			}
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/logout")) {
			request.getSession().invalidate();
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/home.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals(
				applicationContextPath + "/save")) {
			
			
			String rdata=request.getParameter("textarea1");
			String topic=request.getParameter("title_label");
			
			
			
			UserNotes user_notes = new UserNotes();
			
			user_notes.setNotes(rdata);
			user_notes.setTopicID(topic);
			user_notes.setUserId((String)request.getSession().getAttribute("user"));
			
			System.out.println("\n\n Retrived data \n\n " + rdata);
			System.out.println("\n\n Retrived data \n\n " + topic);
			
			if(rdata != null && rdata.isEmpty() == false){
				dbcon.updateUserNotes(user_notes);
				
				Score sc = new Score();
				
				float score = sc.calculateScore(user_notes);
				
				UserInfo user_info = dbcon.getUserInfoByUsername(user_notes.getUserId());
				
				sc.SaveTopicScore(user_info, user_notes.getTopicID(), score);
				
				dbcon.updateUserInfo(user_info);
			}else{
				rdata = "";
			}
			
			
			request.setAttribute("datatext",rdata);
			request.setAttribute("datatitle",topic);
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/dashboard.jsp");
			dispatcher.forward(request, response);
		} 
		
		else if (request.getRequestURI().equals(
				applicationContextPath + "/retrieve")) {
			
			//String topic=request.getParameter("post");
			//String topic = (String)request.getSession().getAttribute("counter");
			String topic = request.getParameter("test");
			System.out.println("\n\ntopic\n\n" + topic);
			request.setAttribute("datatitle",topic);
			
			UserNotes user_notes = dbcon.getUserNotesByUsernameTopicId((String)request.getSession().getAttribute("user"), topic);
		
			if(user_notes == null){
				user_notes = new UserNotes();
				user_notes.setNotes(null);
				user_notes.setTopicID(topic);
			}
			
			
			request.setAttribute("datatext",user_notes.getNotes());
			request.setAttribute("datatitle",user_notes.getTopicID());
			
			System.out.println("datatext" + user_notes.getNotes());
			System.out.println("datatitle" + user_notes.getTopicID());
			
			int topic_id = Simi.getTopicIndex(topic);
			
			request.setAttribute("r_one", Simi.topic_r[topic_id-1][0]);
			request.setAttribute("r_second", Simi.topic_r[topic_id-1][2]);
			request.setAttribute("r_third",Simi.topic_r[topic_id-1][2]);
			
			System.out.println("\n\n Retrived data \n\n " + topic);
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsps/dashboard.jsp");
			dispatcher.forward(request, response);
		} 
		

	}

}
