
package pojo;

public class Login {

	private String userId;
	private String passwd;
	private String email;
	private long id;

	public Login() {
	}

	public Login(String userId, String passwd, String email) {
		this.userId = userId;
		this.passwd = passwd;
		this.email = email;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPasswd() {
		return this.passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
