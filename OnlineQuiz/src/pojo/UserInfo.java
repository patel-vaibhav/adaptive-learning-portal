package pojo;

public class UserInfo {

	private long id;
	private String userId;
	
	private int quizTaken;

	private int quizScore;

	private float ts1;
	private float ts2;
	private float ts3;
	private float ts4;
	private float ts5;
	private float ts6;
	private float ts7;
	private float ts8;
	private float ts9;
	private float ts10;
	private float ts11;
	private float ts12;
	private float ts13;
	private float ts14;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getQuizTaken() {
		return quizTaken;
	}

	public void setQuizTaken(int quizTaken) {
		this.quizTaken = quizTaken;
	}
	
	public int getQuizScore() {
		return quizScore;
	}

	public void setQuizScore(int quizScore) {
		this.quizScore = quizScore;
	}
	
	public float getTs1() {
		return ts1;
	}

	public void setTs1(float ts1) {
		this.ts1 = ts1;
	}

	public float getTs2() {
		return ts2;
	}

	public void setTs2(float ts2) {
		this.ts2 = ts2;
	}

	public float getTs3() {
		return ts3;
	}

	public void setTs3(float ts3) {
		this.ts3 = ts3;
	}

	public float getTs4() {
		return ts4;
	}

	public void setTs4(float ts4) {
		this.ts4 = ts4;
	}

	public float getTs5() {
		return ts5;
	}

	public void setTs5(float ts5) {
		this.ts5 = ts5;
	}

	public float getTs6() {
		return ts6;
	}

	public void setTs6(float ts6) {
		this.ts6 = ts6;
	}

	public float getTs7() {
		return ts7;
	}

	public void setTs7(float ts7) {
		this.ts7 = ts7;
	}

	public float getTs8() {
		return ts8;
	}

	public void setTs8(float ts8) {
		this.ts8 = ts8;
	}

	public float getTs9() {
		return ts9;
	}

	public void setTs9(float ts9) {
		this.ts9 = ts9;
	}

	public float getTs10() {
		return ts10;
	}

	public void setTs10(float ts10) {
		this.ts10 = ts10;
	}

	public float getTs11() {
		return ts11;
	}

	public void setTs11(float ts11) {
		this.ts11 = ts11;
	}

	public float getTs12() {
		return ts12;
	}

	public void setTs12(float ts12) {
		this.ts12 = ts12;
	}

	public float getTs13() {
		return ts13;
	}

	public void setTs13(float ts13) {
		this.ts13 = ts13;
	}

	public float getTs14() {
		return ts14;
	}

	public void setTs14(float ts14) {
		this.ts14 = ts14;
	}
}
