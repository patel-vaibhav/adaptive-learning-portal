
package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import persistence.HibernateUtil;
import pojo.*;

public class DatabaseConnectors {

	public void saveLogin(Login login) {
		 Session session = HibernateUtil.getSessionFactory().openSession();
		 session.beginTransaction();
		 session.save(login);
		 session.getTransaction().commit();
		 session.close();
	}
	
	public void saveUserInfo(UserInfo userinfo) {
		 Session session = HibernateUtil.getSessionFactory().openSession();
		 session.beginTransaction();
		 session.save(userinfo);
		 session.getTransaction().commit();
		 session.close();
	}
	
	public String getPasswdByUsername(String username) {
		 Session session = HibernateUtil.getSessionFactory().openSession();
		 Login login = (Login) session.createCriteria(Login.class)
				 .add( Restrictions.like("userId", username)).uniqueResult();
		 
		 session.close();
		 
		 if(login != null) {
			 return login.getPasswd();
		 }
		 return "";
	}
	
	public int checkLogin(String userid, String passwd){
		String passFromDb = getPasswdByUsername(userid);
		
		if (passFromDb != "" && passwd.equals(passFromDb)){
			return 1;
		}
		return 0;
	}
	
	public Login getLoginByUsername(String username) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Login login = (Login) session.createCriteria(Login.class)
			 .add( Restrictions.like("userId", username)).uniqueResult();
		
		session.close();
		
	 if(login != null) {
		 return login;
	 }
	 return null;
	}
	
	public UserInfo getUserInfoByUsername(String username) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		UserInfo user_info = (UserInfo) session.createCriteria(UserInfo.class)
			 .add( Restrictions.like("userId", username)).uniqueResult();
		
		session.close();
		
	 if(user_info != null) {
		 return user_info;
	 }
	 return null;
	}
	
	public void updateUserInfo(UserInfo userInfo) {
		
		UserInfo tempUserInfo = getUserInfoByUsername(userInfo.getUserId());
		
		userInfo.setId(tempUserInfo.getId());
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		String hql = "delete from UserInfo where username like :uniqId";
		 session.createQuery(hql).setString("uniqId", userInfo.getUserId()).executeUpdate();
		session.save(userInfo);
		session.getTransaction().commit();
		session.close();
	}
	
	public UserNotes getUserNotesByUsernameTopicId(String username, String topicId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		UserNotes usernotes = (UserNotes) session.createCriteria(UserNotes.class).add( Restrictions.and (Restrictions.like("userId", username),Restrictions.like("topicID", topicId))).uniqueResult();
		
		session.close();
		
	 if(usernotes != null) {
		 return usernotes;
	 }
	 return null;
	}
	
	public void updateUserNotes(UserNotes usernotes){
		UserNotes tempUserNotes = getUserNotesByUsernameTopicId(usernotes.getUserId(), usernotes.getTopicID());
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		if(tempUserNotes != null){
			String hql = "delete from UserNotes where username like :uniqId and tid like :topicid";
			 session.createQuery(hql).setString("uniqId", usernotes.getUserId()).setString("topicid", "" + usernotes.getTopicID()).executeUpdate();
		}
		session.save(usernotes);
		session.getTransaction().commit();
		session.close();
	}
	
	public List<UserInfo> getAllUserInfo() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		@SuppressWarnings("unchecked")
		List<UserInfo> results = (List<UserInfo>) session.createCriteria(UserInfo.class).list();
		
		session.close();
		
		if(results != null) {
			return (List<UserInfo>)results;
		}
		return null;
	}
}
