
Read Me file for Adpative Learning Portal

* * * * * * * * * * * * * * * * * * * * 

How to Run:
- Import the Project into Eclipse and Run "Maven install" to get all the dependencies
- Sometimes It causes error for missing Servlet jar file (Please include that)
- Run the Project on Tomcat Server
- It should open the Home page


* * * * * * * * * * * * * * * * * * * * 

System Requirements(Software):

- Eclipse Java EE IDE
- Apache Tomcat 8
- Apache Maven
- MySQL 5.6
- Servlet API
- Apache Lucene
- Jsoup
- Hibernate ORM

* * * * * * * * * * * * * * * * * * * * 

Remarks:

- As Hibernate handles SQL injection by default while saving the data from
notes , we have problems saving those notes containing possible SQL commands
or symbols like - /,"'<:;>*

- When the above scenario happens all the notes disappears from the dashboard sometimes,
Please Sign out from the system and Sign in again to get the Notes back.
 




